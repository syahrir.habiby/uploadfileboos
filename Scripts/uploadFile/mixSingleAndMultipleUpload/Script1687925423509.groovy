import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

TestData testDataPhoto = findTestData('Data Files/pathPhotoData')

WebUI.callTestCase(findTestCase('uploadFile/singleFileUpload'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.openBrowser(GlobalVariable.urlWeb)

for (int rowData = 1; rowData <= testDataPhoto.getRowNumbers(); rowData++) {
    WebUI.uploadFile(findTestObject('uploadFile/chooseFileButton'), testDataPhoto.getValue(2, rowData))

    WebUI.click(findTestObject('uploadFile/uploadButton'))

    WebUI.verifyElementVisible(findTestObject('uploadFile/fileUploaded'))

    WebUI.delay(3)

    WebUI.closeBrowser()

    if (rowData < testDataPhoto.getRowNumbers()) {
        WebUI.openBrowser(GlobalVariable.urlWeb)
    }
}

WebUI.delay(3)

WebUI.closeBrowser()

